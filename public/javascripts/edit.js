const form_edit = document.getElementById('form_edit');

function editData() {
    console.log('~ editData');
    var xmlHttp = new XMLHttpRequest();
    var formData = new FormData(form_edit);
    xmlHttp.open("POST", "/api/user/" + user_id);
    xmlHttp.setRequestHeader('Accept', 'application/json');
    xmlHttp.setRequestHeader('Content-type', 'application/json');
    xmlHttp.onload = function() {
        if (this.status >= 200 && this.status < 300) {
            console.log(this.response);
        } else {
            console.error('Error', this.statusText);
        }
    };
    var data = new Object();
    formData.append('id', user_id);
    formData.forEach(function(value, key) {
        data[key] = value;
    });
    xmlHttp.send(JSON.stringify(data));
    // window.location.href = '/user';
}

form_edit.addEventListener('submit', function(event) {
    editData();
    event.preventDefault();
});
