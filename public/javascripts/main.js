const legendContainer = document.getElementById('legend-container');
const chartContainer = document.getElementById('chart-container');
const shareOverlay = document.querySelector('.share-overlay');
const shareDialog = document.querySelector('.share-dialog');
const closeButton = document.querySelector('.close-button');
const btnToolbar = document.querySelector('.btn-toolbar');
const copyButton = document.querySelector('.copy-link');
const form_save = document.getElementById('form_save');
const month_avg = document.getElementById('month_avg');
const month_lts = document.getElementById('month_lts');
const month = document.getElementsByName('month')[0];
const year = document.getElementsByName('year')[0];
const canvas = document.getElementById('appChart');
const shaUrl = document.getElementById('sha-url');
const faded = document.querySelectorAll('.faded');
const sha = document.getElementById('sha');
const exp = document.getElementById('exp');
const ctx = canvas.getContext('2d');
var socket = io(window.location.origin);

const CHART_COLORS = {
    red: 'rgb(255, 99, 132)',
    orange: 'rgb(255, 159, 64)',
    yellow: 'rgb(255, 205, 86)',
    green: 'rgb(75, 192, 192)',
    blue: 'rgb(54, 162, 235)',
    purple: 'rgb(153, 102, 255)',
    grey: 'rgb(201, 203, 207)'
};

const labels = ['C.S.', 'R.B.', 'Gr', 'Prot', 'Lact', 'Lts', 'Nul'];

function getOrCreateLegendList(chart, id) {
    var listContainer = legendContainer.querySelector('ul');
    if (!listContainer) {
        listContainer = document.createElement('ul');
        listContainer.style.display = 'flex';
        listContainer.style.flexDirection = 'row';
        listContainer.style.margin = 0;
        listContainer.style.padding = 0;
        legendContainer.appendChild(listContainer);
    }
    return listContainer;
}
const htmlLegendPlugin = {
    id: 'htmlLegend',
    afterUpdate(chart, args, options) {
        const ul = getOrCreateLegendList(chart, options.containerID);
        // Remove old legend items
        while (ul.firstChild) {
            ul.firstChild.remove();
        }
        // Reuse the built-in legendItems generator
        const items = chart.options.plugins.legend.labels.generateLabels(chart);
        items.forEach(function(item) {
            const li = document.createElement('li');
            li.style.alignItems = 'center';
            li.style.cursor = 'pointer';
            li.style.display = 'flex';
            li.style.flexDirection = 'row';
            li.style.marginLeft = '10px';
            li.onclick = function(event) {
                if (chart.config === 'pie' || chart.config === 'doughnut') {
                    // Pie and doughnut charts only have a single dataset and visibility is per item
                    chart.toggleDataVisibility(item.index);
                } else {
                    chart.setDatasetVisibility(
                        item.datasetIndex, !chart.isDatasetVisible(item.datasetIndex)
                    );
                }
                chart.tooltip.opacity = 0;
                chart.update();
            };
            // Color box
            const boxSpan = document.createElement('span');
            boxSpan.style.background = item.fillStyle;
            boxSpan.style.borderColor = item.strokeStyle;
            boxSpan.style.borderWidth = item.lineWidth + 'px';
            boxSpan.style.display = 'inline-block';
            boxSpan.style.height = '14px';
            boxSpan.style.marginRight = '6px';
            boxSpan.style.width = '14px';
            // Text
            const textContainer = document.createElement('p');
            textContainer.style.color = item.fontColor;
            textContainer.style.margin = 0;
            textContainer.style.padding = 0;
            textContainer.style.textDecoration = item.hidden ? 'line-through' : '';
            const text = document.createTextNode(item.text);
            textContainer.appendChild(text);
            li.appendChild(boxSpan);
            li.appendChild(textContainer);
            ul.appendChild(li);
        });
    }
};

var appChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: [],
        datasets: [{
                label: labels[0],
                pointBackgroundColor: CHART_COLORS.red,
                backgroundColor: CHART_COLORS.red,
                borderColor: CHART_COLORS.red,
                borderWidth: 4,
                data: []
            },
            {
                label: labels[1],
                pointBackgroundColor: CHART_COLORS.orange,
                backgroundColor: CHART_COLORS.orange,
                borderColor: CHART_COLORS.orange,
                borderWidth: 4,
                hidden: true,
                data: []
            },
            {
                label: labels[2],
                pointBackgroundColor: CHART_COLORS.purple,
                backgroundColor: CHART_COLORS.purple,
                borderColor: CHART_COLORS.purple,
                borderWidth: 4,
                hidden: true,
                data: []
            },
            {
                label: labels[3],
                pointBackgroundColor: CHART_COLORS.green,
                backgroundColor: CHART_COLORS.green,
                borderColor: CHART_COLORS.green,
                borderWidth: 4,
                hidden: true,
                data: []
            },
            {
                label: labels[4],
                pointBackgroundColor: CHART_COLORS.grey,
                backgroundColor: CHART_COLORS.grey,
                borderColor: CHART_COLORS.grey,
                borderWidth: 4,
                hidden: true,
                data: []
            },
            {
                label: labels[5],
                pointBackgroundColor: CHART_COLORS.green,
                backgroundColor: CHART_COLORS.green,
                borderColor: CHART_COLORS.green,
                borderWidth: 4,
                hidden: true,
                data: []
            },
            {
                label: labels[6],
                pointBackgroundColor: CHART_COLORS.yellow,
                backgroundColor: CHART_COLORS.yellow,
                borderColor: CHART_COLORS.yellow,
                borderWidth: 4,
                hidden: true,
                data: []
            }
        ]
    },
    options: {
        events: [
            "click",
            "mouseout",
            "touchstart"
        ],
        responsive: true,
        maintainAspectRatio: true,
        spanGaps: true,
        responsive: true,
        plugins: {
            legend: {
                display: false
            },
            htmlLegend: {
                containerID: 'legend-container'
            }
        },
        scales: {
            x: {
                grid: {
                    drawBorder: false
                }
            },
            y: {
                grid: {
                    drawBorder: false,
                    color: CHART_COLORS.blue
                }
            }
        },
        interaction: {
            intersect: false
        },
        interaction: {
            intersect: false
        },
    },
    plugins: [
        htmlLegendPlugin
    ]
});

var getMonth = new String();
var getYear = new String();
var config = new Object();

function arrSum(arr) {
    var sum = 0;
    arr.forEach(function(val) {
        sum += Number.parseInt(val);
    });
    return sum;
}

function saveData() {
    console.log('~ saveData');
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open('POST', '/api/config-write/');
    xmlHttp.setRequestHeader('Accept', 'application/json');
    xmlHttp.setRequestHeader('Content-type', 'application/json');
    xmlHttp.onload = function() {
        if (this.status >= 200 && this.status < 300) {
            var response = JSON.parse(this.response);
            console.log(response);
            if (response) return loadData();
        } else {
            console.error('Error', this.statusText);
        }
    };
    xmlHttp.send(
        JSON.stringify({
            cows: form_save.cows.value
        })
    );
}

function getConfig(callback) {
    console.log('~ getConfig');
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open('GET', '/api/config-read');
    xmlhttp.onload = function() {
        if (this.status >= 200 && this.status < 300) {
            config = JSON.parse(this.response);
            return callback(config);
        } else {
            console.error('Error', this.statusText);
        }
    };
    xmlhttp.send();
}

function loadDataAnimation(callback) {
    console.log('~ loadDataAnimation');
    setTimeout(function() {
        chartContainer.style.background = 'none';
        callback(true);
    }, 200);
}

function loadData() {
    console.log('~ loadData');
    var xmlhttp = new XMLHttpRequest();
    var options = getYear + '/' + getMonth;
    console.log('~ options', options);
    xmlhttp.open('GET', '/api/items/date/' + options);
    xmlhttp.onload = function() {
        if (this.status >= 200 && this.status < 300) {
            var data = JSON.parse(this.response);
            // console.log(data);
            var tmArray = new Array();
            var csArray = new Array();
            var rbArray = new Array();
            var grArray = new Array();
            var prArray = new Array();
            var laArray = new Array();
            var ltArray = new Array();
            var nuArray = new Array();
            for (var item in data) {
                data[item].map(function(obj) {
                    csArray.push(obj.cs);
                    rbArray.push(obj.rb);
                    grArray.push(obj.gr);
                    prArray.push(obj.pr);
                    laArray.push(obj.la);
                    ltArray.push(obj.lt);
                    nuArray.push(obj.nu);
                    tmArray.push(obj.tm.split('/')[0]);
                });
            }
            getConfig(function(data) {
                if (!data) return;
                m_lts = arrSum(ltArray);
                m_days = tmArray[tmArray.length - 1];
                m_avg = Math.round(m_lts / m_days / data.cows);
                form_save.cows.value = data.cows;
                month_avg.textContent = m_avg;
                month_lts.textContent = m_lts;
            });
            appChart.data.labels = tmArray;
            appChart.data.datasets[0].data = csArray;
            appChart.data.datasets[1].data = rbArray;
            appChart.data.datasets[2].data = grArray;
            appChart.data.datasets[3].data = prArray;
            appChart.data.datasets[4].data = laArray;
            appChart.data.datasets[5].data = ltArray;
            appChart.data.datasets[6].data = nuArray;;
            appChart.update();
            loadDataAnimation(function(callback) {
                if (callback) {
                    console.log('~ loadDataAnimation -> callback');
                    faded.forEach(function(element, index) {
                        setTimeout(function() {
                            element.classList.remove('faded');
                            element.classList.add('fade-in');
                        }, index * 200);
                    });
                }
            });
        } else {
            console.error('Error', this.statusText);
        }
    };
    xmlhttp.send();
}

function getSelected() {
    console.log('~ getSelected');
    getMonth = month.options[month.selectedIndex].value;
    getYear = year.options[year.selectedIndex].value;
    loadData();
}

function loadSelects() {
    console.log('~ loadSelects');
    var objDates = new Object();
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open('GET', '/api/dates');
    xmlhttp.onload = function() {
        if (this.status >= 200 && this.status < 300) {
            objDates = JSON.parse(this.response);
            var count = Object.keys(objDates.rows).length;
            if (count === 0) return console.error("Error: can't get data !");
            objDates.rows.reverse().forEach(function(element) {
                if (year.options.length < count) {
                    var option = document.createElement('option');
                    option.value = option.textContent = element.ye;
                    year.appendChild(option);
                }
                if (year.options[year.selectedIndex].value === element.ye) {
                    element.mo.split(',').reverse().forEach(function(value) {
                        var option = document.createElement('option');
                        option.value = option.textContent = value;
                        month.appendChild(option);
                    });
                }
            });
        } else {
            console.error('Error', this.statusText);
        }
        getSelected();
    };
    xmlhttp.send();
}

socket.on('server_emit', function(data) {
    if (!data) return;
    month.innerHTML = null;
    console.log(data);
    loadSelects();
});

window.addEventListener('load', function() {
    console.log('~ Window load');
    shaUrl.value = window.location.origin;
    form_save.addEventListener('submit', function(event) {
        saveData();
        event.preventDefault();
    });
    year.addEventListener('change', function(event) {
        getYear = event.target.value;
        month.innerHTML = null;
        loadSelects();
        loadData();
    });
    month.addEventListener('change', function(event) {
        getMonth = event.target.value;
        loadData();
    });
    exp.addEventListener('click', function(event) {
        const a = document.createElement('a');
        a.href = appChart.toBase64Image();
        a.download = getMonth + '-' + getYear + '.png';
        a.click();
    });
    sha.addEventListener('click', function(event) {
        document.body.classList.add('is-open');
        shareOverlay.classList.add('is-open');
        shareDialog.classList.add('is-open');
    });
    closeButton.addEventListener('click', function(event) {
        document.body.classList.remove('is-open');
        shareOverlay.classList.remove('is-open');
        shareDialog.classList.remove('is-open');
    });
    copyButton.addEventListener('click', function(event) {
        shaUrl.select();
        shaUrl.setSelectionRange(0, 99999);
        document.execCommand("copy");
    });
    month.innerHTML = null;
    loadSelects();
});