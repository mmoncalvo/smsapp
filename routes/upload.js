var fs = require('fs');
var formidable = require('formidable')
var express = require('express');
var utils = require('../utils');
var xml2js = require('xml2js');
var router = express.Router();
var get_user = new Object();
var is_up_val = false;

function readFilePromise(file) {
    // console.log('readFilePromise');
    return new Promise(function(resolve, reject) {
        return fs.readFile(file, function(err, data) {
            if (err) {
                return reject(err);
            } else {
                xml2js.parseStringPromise(data).then(function(result) {
                    return resolve(result);
                }).catch(function(err) {
                    console.error(err.message);
                });
            }
        });
    });
}

function processFileData(xml) {
    // console.log('processFileData');
    var counter = new Number();
    readFilePromise(xml)
        .then(function(result) {
            var curr_month = new String();
            var objDates = new Object();
            var month = new String();
            var year = new String();
            var mat = get_user.matricula;
            result.smses.sms.forEach(function(element) {
                var body = element['$'].body;
                if (body.split(':')[0] === mat) {
                    if (body.split(':')[1].split(' ')[1] === 'Analisis') {
                        var arrayItems = body.split(',')[2].replace(/=\s+/g, '=').split(' ');
                        var date = body.split(',')[0].split(' ')[4];
                        var lts = body.split(',')[0].split(' ')[6];
                        var arrayValues = new Array(9);
                        arrayValues[0] = date;
                        arrayValues[1] = mat;
                        arrayValues[2] = lts;
                        counter++;
                        arrayItems.forEach(function(item) {
                            var key = item.split('=')[0];
                            var val = item.split('=')[1];
                            if (year !== date.split('/')[2]) {
                                year = date.split('/')[2];
                                objDates[year] = new Array();
                            }
                            if (month !== date.split('/')[1]) {
                                month = date.split('/')[1];
                                objDates[year].push(month);
                            }
                            if (key === 'C.S.') {
                                arrayValues[3] = val;
                            } else if (key === 'R.B.') {
                                arrayValues[4] = val;
                            } else if (key === 'Gr') {
                                arrayValues[5] = val;
                            } else if (key === 'Prot') {
                                arrayValues[6] = val;
                            } else if (key === 'Lact') {
                                arrayValues[7] = val.slice(0, -1);
                            } else if (key === 'NUL') {
                                arrayValues[8] = val.slice(0, -1);
                            }
                            return;
                        });
                        if (curr_month !== month) {
                            curr_month = month;
                        };
                        utils.insertRows(arrayValues, function(bool) {
                            if (!bool) return;
                        });
                    }
                }
            });
            for (var key in objDates) {
                utils.insertDates([key, objDates[key], mat], function(bool) {
                    if (!bool) return;
                });
            }
        })
        .then(function() {
            console.log(counter + ' items created !');
        })
        .catch(function(error) {
            console.error('~ Error: ', error);
        });
}

router.get('/', function(req, res, next) {
    if (req.session.loggedin) {
        get_user = req.session.user;
        utils.getUserById(req.session.user.id, function(data) {
            if (!data) {
                res.status(400).json({ "error": data });
                return;
            }
            res.render('upload', {
                title: 'Upload file',
                user: get_user,
                is_upload: is_up_val
            });
        })
    } else {
        console.log('Please login to view this page!');
        res.redirect('/login');
    }
});

router.get('/last', function(req, res, next) {
    if (req.session.loggedin) {
        fs.readdir(__dirname + '/../public/data/uploads/', function(err, list) {
            var file_name = new String();
            var last_time = new Number();
            list.forEach(function(file) {
                var file_stat = fs.statSync(__dirname + '/../public/data/uploads/' + file);
                var file_time = file_stat.ctime.getTime();
                if (file_time > last_time) {
                    last_time = file_time
                    file_name = file;
                }
            });
            processFileData(__dirname + '/../public/data/uploads/' + file_name);
            console.log('Reload file: ' + file_name);
        	is_up_val = true;
        });
        res.redirect('/upload');
    } else {
        console.log('Please login to view this page!');
        res.redirect('/login');
    }
});

router.post('/', function(req, res) {
    new formidable.IncomingForm().parse(req)
        .on('fileBegin', function(name, file) {
            file.path = __dirname + '/../public/data/uploads/' + file.name;
        })
        .on('file', function(name, file) {
            processFileData(__dirname + '/../public/data/uploads/' + file.name);
            console.log('Uploaded file: ', name, file.name)
			is_up_val = true;
        })
        .on('aborted', function() {
            console.error('Request aborted by the user')
        })
        .on('error', function(err) {
            console.error('Error', err)
            throw err
        })
        .on('end', function() {
            res.end()
        });
    res.redirect('/upload');
})

module.exports = router;
