﻿var fs = require('fs');
var express = require('express');
var router = express.Router();
var utils = require('../utils');
var config = require('../config.json');
var accountSid = config.TWILIO_ACCOUNT_SID;
var token = config.TWILIO_AUTH_TOKEN;
var twilio = require('twilio');
var client = twilio(accountSid, token);
var MessagingResponse = twilio.twiml.MessagingResponse;
var db = utils.getDb(__dirname + '/../public/data/data.db');
var smslog = __dirname + '/../public/data/recive.log';
var is_error = new Boolean();
var get_user = new Object();
var io = new Object();

router.use(function(req, res, next) {
    if (req.session.user) get_user = req.session.user;
    io = req.io.sockets;
    next();
});

fs.watchFile(smslog, function(curr, prev) {
    processFileData(smslog, function(callback) {
        if (callback) {
            console.log(smslog + ' file Changed !!');
            if (Object.keys(io).length > 0) {
                io.emit('server_emit', {
                    message: 'smslog'
                });
            }
        }
    });
});

function readFilePromise(file) {
    // console.log('readFilePromise');
    return new Promise(function(resolve, reject) {
        return fs.readFile(file, function(err, data) {
            if (err) {
                reject(err);
            } else {
                resolve(data.toString().split("\n"));
            }
        });
    });
}

function processFileData(data, callback) {
    console.log('processFileData');
    var counter = new Number();
    readFilePromise(data)
        .then(function(result) {
            var curr_month = new String();
            var objDates = new Object();
            var month = new String();
            var year = new String();
            var mat = new String();
            var mat = get_user.matricula;
            console.log('~ mat', mat);
            result.forEach(function(body) {
                if (body.split(':')[0] === mat) {
                    if (body.split(':')[1].split(' ')[1] === 'Analisis') {
                        var arrayItems = body.split(',')[2].replace(/=\s+/g, '=').split(' ');
                        var date = body.split(',')[0].split(' ')[4];
                        var lts = body.split(',')[0].split(' ')[6];
                        var arrayValues = new Array(9);
                        arrayValues[0] = date;
                        arrayValues[1] = mat;
                        arrayValues[2] = lts;
                        counter++;
                        arrayItems.forEach(function(item) {
                            var key = item.split('=')[0];
                            var val = item.split('=')[1];
                            if (year !== date.split('/')[2]) {
                                year = date.split('/')[2];
                                objDates[year] = new Array();
                            }
                            if (month !== date.split('/')[1]) {
                                month = date.split('/')[1];
                                objDates[year].push(month);
                            }
                            if (key === 'C.S.') {
                                arrayValues[3] = val;
                            } else if (key === 'R.B.') {
                                arrayValues[4] = val;
                            } else if (key === 'Gr') {
                                arrayValues[5] = val;
                            } else if (key === 'Prot') {
                                arrayValues[6] = val;
                            } else if (key === 'Lact') {
                                arrayValues[7] = val.slice(0, -1);
                            } else if (key === 'NUL') {
                                arrayValues[8] = val.slice(0, -1);
                            }
                            return;
                        });
                        if (curr_month !== month) {
                            curr_month = month;
                        }
                        utils.insertRows(arrayValues, function(bool) {
                            if (!bool) return;
                        });
                    }
                }
            });
            for (var key in objDates) {
                utils.insertDates([key, objDates[key], mat], function(bool) {
                    if (!bool) return;
                });
            }
        })
        .then(function() {
            console.log(counter + ' items created !');
            callback(true);
        })
        .catch(function(error) {
            console.error('~ Error: ', error);
        });
}

router.get('/', function(req, res, next) {
    if (req.session.loggedin) {
        res.render('api', {
            title: 'API Page',
            user: req.session.user
        });
    } else {
        console.log('Please login to view this page!');
        res.redirect('/login');
    }
});

router.get('/reload', function(req, res, next) {
    if (req.session.loggedin && (Object.keys(io).length > 0)) {
        processFileData(smslog, function(bool) {
            if (bool) res.redirect('/');
        });
    } else {
        console.log('Please login to view this page!');
        res.redirect('/login');
    }
});

router.get('/config-read', (req, res, next) => {
    // console.log('jsonReader');
    if (config) {
        res.status(200).json(config);
    } else {
        res.status(400).json({
            error: err.message
        });
    }
});

router.post('/config-write', (req, res, next) => {
    console.log('jsonWrite');
    config.cows = new Number(req.body.cows);
    var data = JSON.stringify(config, null, 4);
    fs.writeFile('./config.json', data, function(err) {
        if (err) {
            console.log('Error writing file', err);
            res.status(400).json({
                error: err.message
            });
        } else {
            console.log('Success writing file!');
            res.status(200).json(JSON.parse(data));
        }
    });
});

router.get('/send', (req, res, next) => {
    console.log('Send SMS message !');
    client.messages.create({
            body: 'Hello from Twilio',
            from: '+17244714264',
            to: '+59899760994'
        },
        function(err, result) {
            if (!err) {
                res.status(200).json({
                    message: 'Send SMS message !'
                });
            } else
                res.status(400).json({
                    error: err.message
                });
        }
    );
});

router.post('/sms', (req, res, next) => {
    console.log('POST SMS');
    const twiml = new MessagingResponse();
    twiml.message('The Robots are coming! Head for the hills!');
    res.writeHead(200, { 'Content-Type': 'text/xml' });
    res.end(twiml.toString());
});

// router.get("/login", (req, res, next) => {
//     var sql = "select * from user"
//     var params = []
//     db.all(sql, params, (err, rows) => {
//         if (err) {
//             res.status(400).json({ "error": err.message });
//             return;
//         }
//         res.status(200).json({ rows });
//     });
// });

router.post('/login', function(req, res) {
    var user = req.body.user;
    var password = req.body.password;
    if (user && password) {
        db.all('SELECT * FROM users WHERE user = ? AND password = ?', [
            user,
            password
        ], function(error, results) {
            if (results.length > 0) {
                // const user = JSON.parse(JSON.stringify(results))[0];
                req.session.loggedin = true;
                req.session.user = JSON.parse(JSON.stringify(results))[0];
                // console.log(req.session);
                res.redirect('/user');
            } else {
                req.app.set('message', 'Incorrect Username and/or Password !');
                res.redirect('/login');
                // res.status(400).json({
                // error: 'Incorrect Username and/or Password !'
                // });
            }
        });
    } else {
        req.app.set('message', 'Please enter Username and Password !');
       	res.redirect('/login');
        // res.status(400).json({
            // error: 'Please enter Username and Password !'
        // });
    }
});

router.post('/register', function(req, res) {
    utils.insertUsers([
        req.body.name,
        req.body.user,
        req.body.email,
        req.body.password,
        req.body.matricula
    ], function(bool) {
        if (!bool) {
            console.error("Error to register !");
            req.app.set('is_reg_error', true);
            res.redirect('/register');
        } else {
            req.app.set('is_register', true);
            res.redirect('/login');
        }
    });
});

router.get('/logout', function(req, res, next) {
    req.session.destroy(function(err) {
        console.error('Destroyed session')
    })
    res.redirect('/');
});

router.get("/users", (req, res, next) => {
    var sql = "select * from user"
    var params = []
    db.all(sql, params, (err, rows) => {
        if (err) {
            res.status(400).json({ "error": err.message });
            return;
        }
        res.status(200).json({ rows });
    });
});

router.get("/user/:id", function(req, res, next) {
    utils.getUserById(req.params.id, function(row) {
        if (!row) {
            res.status(400).json({ "error": 'err.message' });
            return;
        }
        res.status(200).json(row);
    });
});

router.get('/dates', (req, res, next) => {
    utils.getDatesByMat(req.session.user.matricula, function(callback) {
        if (!callback) {
            res.status(400).json({ error: 'Error message' });
            return;
        } else {
            res.status(200).json(callback);
        }
    });
});

router.get('/items', (req, res, next) => {
    db.all('SELECT * FROM items', [], (err, rows) => {
        if (err) {
            res.status(400).json({ error: err.message });
            return;
        }
        res.status(200).json({ rows });
    });
});

router.get('/items/:id', (req, res, next) => {
    // var params = [req.params.id]
    db.get(
        'SELECT * FROM items where id = ?', [req.params.id],
        function(err, row) {
            if (err) {
                res.status(400).json({ error: err.message });
                return;
            }
            res.status(200).json(row);
        }
    );
});

router.get('/items/date/:year/:month', (req, res, next) => {
    console.log(req.session.user.matricula);
    db.all(
        'SELECT * FROM items WHERE (substr(tm, 4, 5)) = ? AND (ma) = ? ORDER BY (substr(tm, 1, 2)) ASC', [req.params.month + '/' + req.params.year, req.session.user.matricula],
        function(err, rows) {
            if (err) {
                res.status(400).json({ error: err.message });
                return;
            }
            res.status(200).json({ rows });
        }
    );
});

router.post("/user", (req, res, next) => {
    var errors = new Array();
    var arrData = new Array();
    arrData[0] = req.body.name;
    arrData[1] = req.body.user;
    arrData[2] = req.body.email;
    arrData[3] = req.body.password;
    //  console.log('~ arrData',arrData);        
    if (!req.body.user) {
        errors.push("No user specified");
    } else arrData[1] = req.body.user;
    if (!req.body.email) {
        errors.push("No email specified");
    } else arrData[2] = req.body.email;
    if (!req.body.password) {
        errors.push("No password specified");
    } else arrData[3] = req.body.password;
    if (errors.length) {
        res.status(400).json({ error: errors.join(",") });
        return;
    } else {
        utils.insertUsers(arrData, function(cb) {
            if (!cb) return;
            res.status(200).json(arrData);
        });
    }
})

router.post('/items', (req, res, next) => {
    var reqBody = req.body;
    // console.log('~ reqBody', reqBody);
    db.run(
        'INSERT INTO items (tm, lt, cs, rb, gr, pr, la, nu) VALUES (?,?,?,?,?,?,?,?)', [
            reqBody.tm,
            reqBody.cs,
            reqBody.rb,
            reqBody.gr,
            reqBody.pr,
            reqBody.la
        ],
        function(err) {
            if (err) {
                res.status(400).json({ error: err.message });
                return;
            }
            // res.status(201).json({
            //     "employee_id": this.lastID
            // })
            res.redirect('/api');
        }
    );
});

router.post("/user/:id", function(req, res, next) {
    // console.log(req.body);
    db.run(`UPDATE users set name = ?, user = ?, email = ?, password = ? WHERE id = ?`, [
        req.body.name,
        req.body.user,
        req.body.email,
        req.body.password,
        req.body.id
    ], function(err) {
        if (err) {
            res.status(400).json({ error: res.message });
            return;
        } else {
            res.status(200).json(req.body);
        }
    });
});

router.patch('/items/', (req, res, next) => {
    var reqBody = req.body;
    db.run(
        `UPDATE items set tm = ?, cs = ?, rb = ?, gr = ?, pr = ?, la = ? WHERE id = ?`, [
            reqBody.tm,
            reqBody.cs,
            reqBody.rb,
            reqBody.gr,
            reqBody.pr,
            reqBody.la,
            reqBody.id
        ],
        function(err) {
            if (err) {
                res.status(400).json({ error: res.message });
                return;
            }
            res.status(200).json({ updatedID: this.changes });
        }
    );
});

router.delete('/items/:id', (req, res, next) => {
    db.run(`DELETE FROM items WHERE id = ?`, req.params.id, function(err) {
        if (err) {
            res.status(400).json({ error: res.message });
            return;
        }
        res.status(200).json({ deletedID: this.changes });
    });
});

router.delete('/user/:id', (req, res, next) => {
    db.run(`DELETE FROM users WHERE id = ?`, req.params.id, function(err) {
        if (err) {
            res.status(400).json({ error: res.message });
            return;
        }
        res.status(200).json({ deletedID: this.changes });
    });
});

module.exports = router;
