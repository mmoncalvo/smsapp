var express = require('express');
var utils = require('../utils');
var router = express.Router();

router.get('/', function(req, res, next) {
    if (req.session.loggedin) {
        utils.getUserById(req.session.user.id, function(data) {
            if (!data) {
                res.status(400).json({ "error": data });
                return;
            }
			res.render('edit', {
			    title: 'Edit user account',
			    user: req.session.user
			});
        });
    } else {
        console.log('Please login to view this page!');
        res.redirect('/login');
    }
});

router.post('/', function(req, res, next) {
    if (req.session.loggedin) {
        utils.editUserById(req.session.user.id, function(data) {
            if (!data) {
                res.status(400).json({ "error": data });
                return;
            }
            res.status(200).json(data);
        });
    } else {
        console.log('Please login to view this page!');
        res.redirect('/login');
    }
});

module.exports = router;
