var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
    if (req.session.loggedin) {
        // console.log(req.session);
        res.render('index', {
            title: 'Analisis de remisiones',
            user: req.session.user
        });
    } else {
        console.log('Please login to view this page!');
        res.redirect('/login');
    }
});

module.exports = router;