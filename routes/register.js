﻿var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
    const bool = req.app.get('is_reg_error');
    req.app.set('is_reg_error', false);
    res.render('register', {
        title: 'Register',
        is_reg_error: bool
    });
});

module.exports = router