var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
    const bool = req.app.get('is_register');
    const text = req.app.get('message');
    req.app.set('is_register', false);
    req.app.set('message', false);
    res.render('login', {
        title: 'Login',
        is_register: bool,
        message: text
    });
});

module.exports = router
