var express = require('express');
var utils = require('../utils');
var router = express.Router();

router.get('/', function(req, res, next) {
    if (req.session.loggedin) {
        utils.getUserById(req.session.user.id, function(data) {
            if (!data) {
                res.status(400).json({ "error": data });
                return;
            } else {
                console.log('---------------------------------------------');
                console.log(' User ' + req.session.user.name + ' is logged');
                console.log('---------------------------------------------');
                console.log(' Router name: ', utils.getFileName());
                console.log('---------------------------------------------');
                console.log(req.session.user);
                console.log('---------------------------------------------');
                res.render('user', {
                    title: 'Edit user account',
                    user: req.session.user
                });
            }
        })
    } else {
        console.log('Please login to view this page!');
        res.redirect('/login');
    }
});

module.exports = router;