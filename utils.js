var path = require('path');
var sqlite3 = require('sqlite3').verbose();
var localtunnel = require('localtunnel');
var db = new Object();

module.exports = {
    getDb: function(name) {
        return new sqlite3.Database(name, function(err) {
            if (err) return console.error('Error opening db: ' + err.message);
        });
    },
    createDb: function(path, callback) {
        db = this.getDb(path);
        console.log('Create db: ', path);
        this.createTables(function(bool) {
            if (!bool) return;
        });
        callback(true);
    },
    createTestContent: function(callback) {
        var self = this;
        var arrDates = [
            ["21", "09", "31266"],
            ["21", "09,10", "55555"],
            ["20", "11,12", "55555"]
        ];
        var arrUsers = [
            ["Testuser", "test", "test@example.com", "test", "55555"]
        ];
        var arrItems = [
            ["02/09/21", "31266", "1890", "193000", "20000", "3.21", "3.20", "4.24", "10.1"],
            ["08/09/21", "31266", "1790", "200000", "0", "3.12", "3.36", "4.30", "12.2"],
            ["16/09/21", "31266", "1600", "230000", "0", "3.92", "3.44", "4.61", "16.4"],
            ["22/09/21", "31266", "1940", "200000", "0", "3.19", "3.48", "4.80", "14.6"],
            ["28/09/21", "31266", "1492", "222000", "18000", "3.40", "3.40", "4.20", "11.8"],
            ["30/09/21", "31266", "1890", "100000", "24000", "3.20", "3.30", "4.46", "13.6"],
            ["08/11/20", "55555", "1940", "193000", "0", "3.19", "3.48", "4.80", "14.6"],
            ["16/11/20", "55555", "1790", "093000", "0", "3.12", "3.36", "4.30", "12.2"],
            ["14/11/20", "55555", "1890", "300000", "0", "3.21", "3.20", "4.24", "10.1"],
            ["29/11/20", "55555", "1600", "232000", "10000", "3.92", "3.44", "4.61", "16.4"],
            ["01/12/20", "55555", "1940", "193000", "0", "3.19", "3.48", "4.80", "14.6"],
            ["06/12/20", "55555", "1790", "093000", "0", "3.12", "3.36", "4.30", "12.2"],
            ["14/12/20", "55555", "1890", "300000", "0", "3.21", "3.20", "4.24", "10.1"],
            ["26/12/20", "55555", "1600", "232000", "10000", "3.92", "3.44", "4.61", "16.4"],
            ["02/09/21", "55555", "1940", "193000", "0", "3.19", "3.48", "4.80", "14.6"],
            ["08/09/21", "55555", "1790", "093000", "0", "3.12", "3.36", "4.30", "12.2"],
            ["16/09/21", "55555", "1890", "300000", "0", "3.21", "3.20", "4.24", "10.1"],
            ["19/09/21", "55555", "1600", "232000", "10000", "3.92", "3.44", "4.61", "16.4"],
            ["23/09/21", "55555", "1890", "230000", "0", "3.20", "3.30", "4.46", "13.6"],
            ["28/09/21", "55555", "1492", "243000", "18000", "3.40", "3.40", "4.20", "11.8"],
            ["30/09/21", "55555", "1592", "260000", "0", "3.40", "3.40", "4.20", "11.8"],
            ["01/10/21", "55555", "1492", "203000", "18000", "3.40", "3.40", "4.20", "11.8"],
            ["04/10/21", "55555", "1592", "200000", "0", "3.40", "3.40", "4.20", "11.8"],
            ["18/10/21", "55555", "1492", "243000", "18000", "3.40", "3.40", "4.20", "11.8"],
            ["28/10/21", "55555", "1592", "260000", "0", "3.40", "3.40", "4.20", "11.8"]
        ];
        arrItems.forEach(function(arr) {
            self.insertRows(arr, function(cb) {
                if (!cb) return;
                console.log('# Test item created !');
            });
        });
        arrDates.forEach(function(arr) {
            self.insertDates(arr, function(cb) {
                if (!cb) return;
                console.log('# Test date created !');
            });
        });
        arrUsers.forEach(function(arr) {
            self.insertUsers(arr, function(cb) {
                if (!cb) return;
                console.log('# Test user created !');
            });
        });
        callback(true);
    },
    createTables: function(callback) {
        console.log('Create default tables');
        db.serialize(function() {
            db.run(
                'CREATE TABLE items (\
					id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,\
					tm NVARCHAR(20) NOT NULL,\
					ma NVARCHAR(20) NOT NULL,\
					lt NVARCHAR(20),\
					cs NVARCHAR(20),\
					rb NVARCHAR(20),\
					gr NVARCHAR(20),\
					pr NVARCHAR(20),\
					la NVARCHAR(20),\
					nu NVARCHAR(20)\
				)',
                function(err) {
                    if (err) {
                        console.error('---------------------------------------------');
                        console.error('Table elements already exist, or have errors	');
                        console.error('---------------------------------------------');
                        console.error(err.message);
                    } else {
                        console.log('Table items created.');
                    }
                }
            );
            db.run(
                'CREATE TABLE dates (\
					id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,\
					ye NVARCHAR(20) NOT NULL,\
					mo NVARCHAR(20) NOT NULL,\
					ma NVARCHAR(20) NOT NULL\
				)',
                function(err) {
                    if (err) {
                        console.error('---------------------------------------------');
                        console.error('Table elements already exist, or have errors	');
                        console.error('---------------------------------------------');
                        console.error(err.message);
                    } else {
                        console.log('Table dates created.');
                    }
                }
            );
            db.run(
                `CREATE TABLE users (\
					id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,\
					name NVARCHAR(20) NOT NULL,\
					user NVARCHAR(20) UNIQUE NOT NULL,\
					email NVARCHAR(20) UNIQUE NOT NULL,\
					password NVARCHAR(20) NOT NULL,\
					matricula NVARCHAR(20) NOT NULL\
				)`,
                function(err) {
                    if (err) {
                        console.error('---------------------------------------------');
                        console.error('Table elements already exist, or have errors	');
                        console.error('---------------------------------------------');
                        console.error(err.message);
                    } else {
                        console.log('Table users created.');
                    }
                }
            );
        });
        callback(true);
    },
    getUserById: function(id, callback) {
        var sql = "select * from users where id = ?";
        db.get(sql, id, function(err, row) {
            if (err) {
                console.error(err.message);
                callback(false);
            } else callback(true);
        });
    },
    getDatesByMat: function(mat, callback) {
        var sql = "SELECT * FROM dates where ma = ?";
        db.all(sql, mat, function(err, rows) {
            if (err) {
                console.error(err.message);
                callback(false);
            } else callback({ rows });
        });
    },
    getFileName: function() {
        return path.basename(__filename);
    },
    insertRows: function(vals, callback) {
        // console.log(" - Insert rows in " + name);
        var arr_str = ("'" + vals.join("','") + "'");
        var tm_str = ("'" + vals[0] + "'");
        var ma_str = ("'" + vals[1] + "'");
        var insert = `INSERT INTO items (tm, ma, lt, cs, rb, gr, pr, la, nu)\
		SELECT * FROM (SELECT ${arr_str}) AS tmp\
		WHERE NOT EXISTS (\
			SELECT tm FROM items WHERE tm = ${tm_str} AND ma = ${ma_str}\
			) LIMIT 1`;
        db.run(insert, [], function(err) {
            if (err) {
                console.error(err);
                callback(false);
            } else callback(true);
        });
    },
    insertDates: function(vals, callback) {
        console.log(" - Insert dates in dates table");
        console.log('~ vals', vals);
        var arr_str = ("'" + vals.join("','") + "'");
        var ye_str = ("'" + vals[0] + "'");
        var mo_str = ("'" + vals[1] + "'");
        var ma_str = ("'" + vals[2] + "'");
        var insert = `INSERT INTO dates (ye, mo, ma)\
		SELECT * FROM (SELECT ${arr_str}) AS tmp\
		WHERE NOT EXISTS (\
			SELECT ye, mo, ma FROM dates WHERE instr(mo, ${mo_str}) > 0 AND ye = ${ye_str} AND ma = ${ma_str}\
		) LIMIT 1`;
        db.run(insert, [], function(err) {
            if (err) {
                console.error(err.message);
                callback(false);
            } else callback(true);
        });
    },
    insertUsers: function(vals, callback) {
        // console.log(" - Insert users in users table");
        var insert = 'INSERT INTO users (name, user, email, password, matricula) VALUES (?,?,?,?,?)'
        db.run(insert, vals, function(err) {
            if (err) {
                console.error(err.message);
                callback(false);
            } else callback(true);
        });
    },
    editUserById: function(array, callback) {
        var sql = "UPDATE users set name = ?, user = ?, email = ?, password = ?, matricula = ? WHERE id = ?";
        db.run(sql, array, function(err, row) {
            if (err) {
                console.error(err.message);
                callback(false);
            } else callback(row);
        });
    },
    closeDb: function(callback) {
        console.log('Close db');
        db.close();
        callback(true);
    },
    tunnel: async function(sdom, port = 8080) {
        const tunnel = await localtunnel(port, {
            subdomain: sdom || 'devapp'
        });
        console.table({
            Server: {
                Url: tunnel.url,
                Port: port
            }
        });
        tunnel.on('close', function() {
            console.log('tunnels are closed');
        });
    }
};