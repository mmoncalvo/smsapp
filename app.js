﻿var registerRouter = require('./routes/register');
var cookieParser = require('cookie-parser');
var uploadRouter = require('./routes/upload');
var indexRouter = require('./routes/index');
var loginRouter = require('./routes/login');
var userRouter = require('./routes/user');
var editRouter = require('./routes/edit');
var session = require('express-session');
var createError = require('http-errors');
var apiRouter = require('./routes/api');
var bodyParser = require('body-parser');
var express = require('express');
var logger = require('morgan');
var utils = require('./utils');
var path = require('path');
var http = require('http');
var app = express();
var server = http.createServer(app);
var io = require('socket.io')(server);
var port = 8080;

app.use(session({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: true
}));

app.use(function(req, res, next) {
    const user = req.session.user;
    if (!user) req.session['user'];
    req.io = io;
    next();
});

app.use(logger('dev'));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'node_modules/eruda')));
app.use(express.static(path.join(__dirname, 'node_modules/chart.js/dist')));
app.use(express.static(path.join(__dirname, 'node_modules/bootstrap-icons/font')));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'twig');
app.set('json spaces', 2);

// routes setup
app.use('/', indexRouter);
app.use('/api', apiRouter);
app.use('/edit', editRouter);
app.use('/user', userRouter);
app.use('/login', loginRouter);
app.use('/register', registerRouter);
app.use('/upload', uploadRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

utils.createDb('./public/data/data.db', function(bool) {
    if (bool) { 
        var arrUsers = [
            ["Administrator", "admin", "admin@example.com", "admin", "31266"]
        ];
        arrUsers.forEach(function(arr) {
            utils.insertUsers(arr, function(cb) {
                if (!cb) return;
                console.log('# Test user created !');
            });
        });
        // utils.createTestContent(function() {
        //     const options = new URL('http://127.0.0.1/api/reload');
        //     http.get(options, function(res) {
        //         if (!res) return;
        //         console.log('---------------------------------------------');
        //         console.log(' NOW RELOAD DATABASE !						  ');
        //         console.log('---------------------------------------------');
        //     });
        // });
    }
});

server.listen(port, function() {
    console.log('Server start at: ' + port + '.');
});

io.on('connection', function(socket) {
    console.log('Some client connected')
});

module.exports = app;